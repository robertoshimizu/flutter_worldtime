
import 'package:flutter/material.dart';
import 'package:flutterworldtime/pages/choose_location.dart';
import 'package:flutterworldtime/pages/home.dart';
import 'package:flutterworldtime/pages/loading.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/home', // This overrides the order below, just for test purposes
    routes:{
      '/': (context) => Loading(),
      '/home': (context) => Home(),
      '/location': (context) => ChooseLocation(),
    },
  ));
}
