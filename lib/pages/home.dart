import 'package:flutter/material.dart';

// Home is a Stateful widget, which links a State object with the _HomeState widget
// Inside the State object we have a build function with returns the Widget tree

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            // to bring the elements out of the top bar
            child: Column(
              children: <Widget>[
                FlatButton.icon(
                    onPressed: () {
                      Navigator.pushNamed(context, '/location');
                    },
                    icon: Icon(Icons.edit_location),
                    label: Text('Edit Location')
                ),
              ],
            )
        )
    );
  }
}
